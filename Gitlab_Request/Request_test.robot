*** Settings ***
Library           RequestsLibrary

*** Test Cases ***
Request1
    create session    session    https://www.mi.com
    ${header}    create dictionary    Content-Type=application/x-www-form-urlencoded
    ${req}    get request    session    /search?keyword=小米笔记本 15.6    headers=${header}
    log    ${req}

Request2
    create session    session    https://www.mi.com
    ${header}    create dictionary    Content-Type=application/x-www-form-urlencoded
    ${req}    get request    session    /buy/detail?product_id=10000239    headers=${header}
    log    ${req}
    Comment    log    ${req.text}

Request3
    create session    session    https://www.mi.com
    ${header}    create dictionary    Content-Type=application/x-www-form-urlencoded
    ${req}    get request    session    /buy/detail?product_id=11843    headers=${header}
    log    ${req}
    Comment    log    ${req.text}

Request4
    create session    session    https://www.mi.com
    ${header}    create dictionary    Content-Type=application/x-www-form-urlencoded
    ${req}    get request    session    /search?keyword=手表    headers=${header}
    log    ${req}
    Comment    log    ${req.text}

Request5
    create session    session    https://www.mi.com
    ${header}    create dictionary    Content-Type=application/x-www-form-urlencoded
    ${req}    get request    session    /miwatch/color-keith?product_id=1195200002&cfrom=search    headers=${header}
    log    ${req}
    Comment    log    ${req.text}
